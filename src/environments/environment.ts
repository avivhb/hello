// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBwgSxYPk5Bk1bYB1WCSDq_8U9fCUuRfQE",
    authDomain: "hello-aviv.firebaseapp.com",
    databaseURL: "https://hello-aviv.firebaseio.com",
    projectId: "hello-aviv",
    storageBucket: "hello-aviv.appspot.com",
    messagingSenderId: "44840853974",
    appId: "1:44840853974:web:acba26b3ac36f5e5a48477"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
