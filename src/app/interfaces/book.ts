export interface Book {
    id:string,
    title:string,
    author: string,
    dsummary?:string
}
