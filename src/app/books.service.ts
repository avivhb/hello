import { ObserversModule } from '@angular/cdk/observers';
import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  books = [
  {title:'Alice in Wonderland', author:'Lewis Carrol', summary:'1'},
  {title:'War and Peace', author:'Leo Tolstoy', summary:'2'}, 
  {title:'The Magic Mountain', author:'Thomas Mann', summary:'3'}
  ];

// create new book each time
public addBooks(){
  setInterval(() => this.books.push({title:'A new one', author:'New author', summary:'n'}),2000);
}

// push method with observable
/*public getBooks(){
  const booksObservable = new Observable(observer => {
    setInterval(() => observer.next(this.books),500)
  });
  return booksObservable;
}
*/

bookCollection:AngularFirestoreCollection;
userCollection:AngularFirestoreCollection = this.db.collection('users');

// data {title,author,id}
public getBooks(userId,startAfter){
  this.bookCollection = this.db.collection(`users/${userId}/books`, 
  ref => ref.orderBy('title','asc').limit(4).startAfter(startAfter));
  return this.bookCollection.snapshotChanges()
}

/*
public getBooks(userId){
  this.bookCollection = this.db.collection(`users/${userId}/books`);
  return this.bookCollection.snapshotChanges().pipe(map(
    collection => collection.map(
      document => {
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        return data;
      }
    )
  ))
} */

deleteBook(userId:string,id:string){
  this.db.doc(`users/${userId}/books/${id}`).delete();
}

addBook(userId:string,title:string,author:string){
  const book = {title:title, author:author};
  this.userCollection.doc(userId).collection('books').add(book);
}

updateBook(userId:string,id:string,title:string,author:string){
  this.db.doc(`users/${userId}/books/${id}`).update(
    {
      title:title,
      author:author
    }
  )
}



  
/* Pull method
  public getBooks(){
    return this.books;
  } 
*/



// getBooks(userId):Observable<any[]>{

// }

  constructor(private db:AngularFirestore) { }
}
