import { WeatherService } from './../weather.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/weather';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css'],
  
})
export class TemperaturesComponent implements OnInit {
  weatherData$:Observable<Weather>;
  hasError:boolean = false;
  errorMessage:string;
  name:string;
  country:string;
  image:string;
  description:string;
  temperature:number;
  city:string;
  lat:number;
  lon:number;
    
  constructor(private route:ActivatedRoute, private WeatherService:WeatherService) { }

  ngOnInit(): void {
    this.city = this.route.snapshot.params.city;
    this.weatherData$ = this.WeatherService.searchWeatherData(this.city);
    this.weatherData$.subscribe(
      data => {
        this.name = data.name;
        this.country = data.country;
        this.image = data.image;
        this.description = data.description;
        this.temperature = data.temperature;
        this.lat = data.lat;
        this.lon = data.lon;
      },
      error => {
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message;
      }
    )
  }

}
